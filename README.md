 # Table des matières
 - [Préparation du modèle 3D à imprimer avec le logiciel CURA](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#pr%C3%A9paration-mod%C3%A8le-3d-%C3%A0-imprimer-avec-le-logiciel-cura)
 - Impression sur place en mode manuel
    - [Transfert/Insertion du modèle 3D imprimable dans l'imprimante 3D](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#transfertinsertion-du-mod%C3%A8le-3d-imprimable-dans-limprimante-3d)
    - [Initialisation et préparation de l'imprimante 3D](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#initialisation-et-pr%C3%A9paration-de-limprimante-3d)
    - [Calibration Imprimante 3D](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#calibration-imprimante-3d)
    - [Lancement impression 3D](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#lancement-impression-3d)
 - Impression via application web `Octoprint`
    - [Accès web à l'application Octoprint](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#acc%C3%A8s-web-%C3%A0-lapplication-octoprint)
    - [Authentification Octoprint via http://octopi.local](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#authentification-octoprint-via-httpoctopilocal)
    - [Connection à l'imprimante depuis Octoprint](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#connection-%C3%A0-limprimante-depuis-octoprint
)
    - [Transfert du modèle 3D depuis Octoprint](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#transfert-du-mod%C3%A8le-3d-depuis-octoprint
)
    - [Initialisation et préparation de l'imprimante 3D depuis Octoprint](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#initialisation-et-pr%C3%A9paration-de-limprimante-3d-depuis-octoprint
)
    - [Lancement impression 3D depuis Octoprint](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#lancement-impression-3d-depuis-octoprint
)
    - [Surveillance depuis Octoprint](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#surveillance-depuis-octoprint
)
    - [Deconnection Octoprint et coupure imprimante](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#deconnection-octoprint-et-coupure-imprimante-%C3%A0-distance
)
  - Gestion d'une impression en cours depuis internet via l'application web `Home assistant`
    - [Accès web à l'application Octoprint depuis Home Assistant](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#acc%C3%A8s-%C3%A0-octoprint-depuis-home-assistant-via-httpspastelfablabduckdnsorg8123)
    - [Surveillance de l'impression depuis Home Assistant](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#surveillance-de-limpression-depuis-home-assistant)
    - [ARRET D'URGENCE EN CAS DE PROBLEME depuis Home Assistant](https://gitlab.com/pastel-fablab/fablab-tools/fiche-imprimante-3d/-/blob/master/README.md#arret-durgence-en-cas-de-probl%C3%A8me)

 # Préparation modèle 3D à imprimer avec le logiciel CURA
 1.  Lancer le logiciel CURA depuis un des PC Windows du FabLab
 2.  Charger le fichier modèle 3D à imprimer (format STL)
 3.  Vérifier dans le panneau droit le matériau sélectionné (PLA)
 4.  Paramétrer les conditions d'impression dans le panneau droit `a minima` (via mode recommandé ou personnalisé) :
 
 - [ ]  la hauteur entre couche (`profile`) : ex : 0.12
 - [ ]  le taux de remplissage (`infill`) : ex : 20%
 - [ ]  la température de la buse (`printing temperature`) : ex : 195°C
 - [ ]  la température du plateau (`build plate temperature`) : ex : 60°C
 
 ![Alt text](/images/ChoixTemperature.JPG?raw=true "Choix des conditions d'impression")

 5.  Lancer la préparation du modèle via le bouton en bas à droite **Prepare**
 > Une indication du temps estimé d'impression, longueur de fil et cout est affichée<p></p>
 > Si le temps estimé est trop important, revenir à l'étape 4 et jouer sur les conditions d'impression
 6.  Enregistrer le modèle3D imprimable (extension `.gcode`) via le bouton en bas à droite **Save to file...**

 # Impression sur place en mode manuel
 ## Transfert/Insertion du modèle 3D imprimable dans l'imprimante 3D 
 1.  Copier le modèle3D imprimable (extension `.gcode`) sur la carte SD via le dongle prévu à cet effet
 2.  Insérer la carte SD dans l'imprimante 3D (dans le lecteur en bas a gauche sur la facade avant de l'appareil)
  
 ![Alt text](/images/Dongle.JPG?raw=true "Dongle de transfert sur carte SD")

 
 ## Initialisation et préparation de l'imprimante 3D
 1. Allumer l'imprimante (bouton sur la facade droite de l'appareil)
 2. Enlever le surplus de fil de l'extruder avec la pince coupante
 3. Cliquer s#ur le bouton rotatif du panneau de commande 
 4. Sélectionner ***Menu/Prepare/Auto Home*** puis revenir au menu ***Main***
 5. Faire chauffer le plateau ***Menu/Control/Temperature/Bed***. Choisir une temperature de 60°C puis revenir au menu ***Main***
 6. Attendre que le plateau atteigne la température de consigne en surveillant les indications du panneau LCD de l'imprimante

 ![Alt text](/images/BedHeating.JPG?raw=true "Surveillance de la temperature de consigne du plateau")

 
 ## Calibration Imprimante 3D
 A faire à chaud, c.à.d avec le lit et la buse aux tempèratures réglées dans votre slicer pour l'impression de votre pièce.
 > Sur la carte SD est pré-enregistré un programme destiné à faciliter la calibration de l'imprimante ***calibration_ender3.gcode***
 1. Cliquer sur le bouton rotatif du panneau de commande 
 2. Sélectionner  ***Menu/Print from SD***, choisir le fichier ***calibration_ender3.gcode*** puis revenir au menu ***Main***
 > La tête d'impression de l'imprimante se positionne automatiquement sur le coin bas/gauche du plateau.
 3. Glisser une feuille de papier sous la tête d'impression 
 
 ![Alt text](/images/CalibrationAvecFeuille.JPG?raw=true "Calibration avec une feuille de papier")
 
 4. S'assurer que le papier n'est ni trop lâche, ni trop retenu par la pointe de la tête de lecture.
 5. Régl#er la hauteur du plateau de la position concernée à l'aide de la molette située sous le plateau jusqu'a ce que la condition précédente soit respecteé
 6. Confirmer le bon réglage et passer à la position de calibration suivante en sélectionant ***Click to Resume...***
 > La tête d'impression va faire un parcours de calibration consistant à calibrer les 4 coins et le centre du plateau.<p></p> 
 > Ce parcours est effectué ***deux fois*** afin de pouvoir consolider les réglages

 
 ## Lancement impression 3D 
 1. Cliquer sur le bouton rotatif du panneau de commande 
 2. Sélectionner  ***Menu/Print from SD***, choisir le fichier modèle3D imprimable `.gcode` à imprimer puis revenir au menu ***Main***
 3. Surveiller que la buse d'impression atteint bien la température de consigne configurée dans le modèle en consultant les indications du panneau LCD de l'imprimante
 > La tête d'impression une fois la température atteinte, trace un trait vertical le long du plateu pour nettoyer la buse, puis une bounding box représentant l'emprise de la pice
 4. Verifier que la bounding box c'est bien tracée, est bien centrée et que le filament déposé est bien collée au plateau

 ![Alt text](/images/DemarrageImpression.JPG?raw=true "Controle de centrage et de dépot du filament")

 5. Si l'étape 4 n'est pas correcte. Arreter l'impression. Sélectionner  ***Menu/????***, 
 6. Si l'étape 4 est correcte, laisser ce dérouler l'impression. Le temps ***passé*** et non ***restant*** est affiché sur le panneau d'affichage de l'imprimante ainsi que le ***pourcentage d'avancement***
 
 `BONNE IMPRESSION` et surtout  `PATIENCE`

 # Impression via application web Octoprint
 ## Accès web à l'application Octoprint
  Deux possibilités pour se connecter à Octoprint:
  - Pour lancer une impression, UNIQUEMENT depuis le local FabLab, via le reseau wifi interne http://octopi.local et un PC du FabLab
  - Pour surveiller l'impression, depuis N'IMPORTE OU via l'extranet et Home Assistant
 ## Authentification Octoprint via http://octopi.local
   - La fenetre d'authentification Octoprint suivante apparait:
   ![Alt text](/images/Octoprint-Login.JPG?raw=true "Fenetre de login Octoprint")
   - Mettre le curseur dans la zone de texte ***username***
   - Lancer keepass depuis le menu démarrer de Windows, sélectionner octoprint  
   ![Alt text](/images/Octoprint-KeePass.JPG?raw=true "Fenetre Keepass)
   - Taper `Control+V`, le user/password Octoprint nécessaire se transfère et initialise automatiquement la fenêtre d'authentification Octoprint
   - Cliquer sur le bouton **Log in** pour terminer l'authentification
 
 ## Connection à l'imprimante depuis Octoprint
   - Etre authentifié (voir item précédent)
   - Dans le panneau gauche déplier le premier sous-panneau ***Connection***:
     ![Alt text](/images/Octoprint-connect.JPG?raw=true "Fenetre de connexion Octoprint")

   - Cliquer sur le bouton ***Connect***
 ## Transfert du modèle 3D depuis Octoprint
   - Etre authentifié (voir item précédent)
   - Dans le panneau gauche déplier le premier sous-panneau ***Files***:
     ![Alt text](/images/Octoprint-FileSelect.JPG?raw=true "Fenetre de selection fichier Octoprint")
     - Soit uploader un fichier depuis votre poste distant (bouton upload)
     - Soit sélectionner un fichier existant (voir liste proposée ou search)

 ## Initialisation et préparation de l'imprimante 3D depuis Octoprint
   - Aller dans l'onglet ***temperature*** :
   ![Alt text](/images/Octoprint-temperatureControl.JPG?raw=true "Fenetre de controle temperature Octoprint")
   - Faire chauffer le plateau ***Bed*** . Choisir une temperature de 60°C et activer la résiatnce de chauffage via le bouton checker bleu
   - Attendre que le plateau atteigne la température de consigne en surveillant les indications et courbes affichées dans l'onglet
 ## Lancement impression 3D depuis Octoprint
   - Dans le panneau gauche déplier le premier sous-panneau ***State***:
     ![Alt text](/images/Octoprint-print.JPG?raw=true "Fenetre de pilotage impression Octoprint")
   - Cliquer sur le bouton ***Print*** pour lancer l'impression
   - Cliquer sur le bouton ***Pause*** pour suspendre l'impression
   - Cliquer sur le bouton ***Cancel*** pour annuler l'impression
 ## Surveillance depuis Octoprint
   - Aller dans l'onglet ***control*** :
   ![Alt text](/images/Octoprint-CameraControl.JPG?raw=true "Fenetre de controle caméra Octoprint")
   > Nota : ce panneau permet également de piloter la tete manuellement a distance (en phase de préparation par exemple) ainsi que de controler l'état des moteurs et ventilateurs
 ## Deconnection Octoprint et coupure imprimante à distance
   - Depuis l'application Octoprint deconnectez vous de l'imprimante
   - Attendez que le plateau et la tête refroidisse
   - Deauthentifiez vous de l'application Octoprint
   - Coupez l'alimentation de l'imprimante

 ## Accès à Octoprint depuis Home Assistant via https://pastelfablab.duckdns.org:8123
   - La fenetre d'authentification Home Assistant apparait:
     - Authentifiez vous avec votre user/password ou utilisez l'application keepass pour effectuer une authentification simplifiée
     - Aller dans l'onglet ***Supervisor/Dashboard/Addons*** et selectionner l'addon ***Duck Dns***
     ![Alt text](/images/HomeAssistant-OctoprintAccès.JPG?raw=true "Mecanisme d'accès à Octoprint depuis Home assistant")
     - Selectionner octoprint et authentifiez vous ???? (TBC)

 ## Surveillance de l'impression depuis Home Assistant
 - Aller dans l'onglet ***FabLab*** et selectionner dans la barre d'icone le troisième icone (tooltips imprimante)
 - Selectionner ***Ender 3d*** la fenêtre de suivi d'impression Octoprint suivante apparait:
 ![Alt text](/images/HomeAssistant-OctoprintSurveillance.JPG?raw=true "Suivi impression Octoprint depuis Home assistant")

 ## Arret d'urgence en cas de problème
   - Aller dans l'onglet ***FabLab*** et selectionner dans la barre d'icone le deuxième icone (tooltips prise)
   - Decocher le bouton ***Prise ENDER3*** qui coupe complètement l'alimentation via la prise pilotée a distance WireGuard
      ![Alt text](/images/HomeAssistant-OctoprintStopPrinting.JPG?raw=true "Arret d'urgence  impression Octoprint depuis Home assistant")

 
 